  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.init();
  },
/**
 * @desc 初始化函数
 */
  init() {
    // 判断用户是否登录，如果没有登录的话，是没有购物车信息的
    const app = getApp();
    console.log("APP", app);
    if(app.globalData.userInfo == undefined || app.globalData.userInfo == '') {
      this.setData({
        none: 'none'
      })
    } else {
      this.setData({
        none: 'block'
      })
    }
  }