// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    banners: [],
    categorys: [],
    recommends: [],
    pageNum:1,
    total:0,
    loading:'loading...',
    news: [],
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  getBanners(){
    wx.request({
      url: 'https://obuge.com/juzao/index/banners', 
      success:res =>{
        let banners = res.data
        for (let i=0;i<banners.length;i++){
          banners[i].img = 'https://obuge.com/juzao' + banners[i].img
        }
        this.setData({
          banners:banners
        })
      }
    })
  },
  getCategorys(){
    wx.request({
      url: 'https://obuge.com/juzao/index/categorys', 
      success:res =>{
        let categorys = res.data
        for (let i=0;i<categorys.length;i++){
          categorys[i].img = 'https://obuge.com/juzao' + categorys[i].img
        }
        this.setData({
          categorys:categorys
        })
      }
    })
  },
  getNews(){
    wx.request({
      url: 'https://obuge.com/juzao/index/news', 
      success:res =>{
        let news = res.data
        for (let i=0;i<news.length;i++){
          news[i].title 
        }
        this.setData({
          news:news
        })
      }
    })
  },
  //跳转界面
  goto(event){
    let url = event.currentTarget.dataset.url
    console.log(event,url)
    wx.showModal({
      title: '跳转路径',
      content: url,
      success (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  getRecommends(){
    wx.request({
      url: 'https://obuge.com/juzao/index/recommends', 
      data:{
        pageNum:this.data.pageNum
      },
      success:res =>{
        let recommends = this.data.recommends
        let newCommends = res.data.rows
        for (let i = 0;i<newCommends.length;i++){
          newCommends[i].img = 'https://obuge.com/juzao' + newCommends[i].img
          recommends.push(newCommends[i])
        }
        this.setData({
          total:res.data.total,
          recommends:recommends
        })
      }
    })
  },
  onReachBottom(){
    if(this.data.recommends.length < this.data.total){
      this.data.pageNum++
      this.getRecommends()
    }
  },  
  getList(){
    Promise.all([this.getNews(),this.getBanners(),this.getCategorys()]).then(()=>{
      console.log("所有数据加载完成")
    }).catch(e =>{
      console.log(e)
    })

  }
,
  onLoad() {
    this.getBanners()
    this.getRecommends()
    this.getCategorys()
    this.getNews()
    this.getList()

    //this.getCategorys()
    wx.login({
      
    })
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
